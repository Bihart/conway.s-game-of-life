#include <stdlib.h>
#include <time.h>
#include <stdio.h>

#include <SDL2/SDL.h>

#define QUITKEY SDLK_ESCAPE
#define N 35
#define SCREEN_WIDTH  1024
#define SCREEN_HEIGHT 1024
#define CELL_WIDTH  (SCREEN_WIDTH / N)
#define CELL_HEIGHT (SCREEN_HEIGHT / N)

SDL_Window *screen = NULL;
SDL_Renderer *renderer;
SDL_Event event;

int keypressed, rectCount = 0;

int
min(int a, int b){ return a < b ? a : b; }

void
LogError(char * msg){ printf("%s\n", msg); }

void
SetCaption(char * msg){ SDL_SetWindowTitle(screen, msg); }

void
DrawGrid()
{
    SDL_SetRenderDrawColor(renderer, 120, 120, 120, 255); /* Grey */

    for(int i = 1; i < N; i++)
    {
        SDL_RenderDrawLine(
            renderer,
            i * CELL_WIDTH, 0,
            i * CELL_WIDTH, SCREEN_HEIGHT);

        SDL_RenderDrawLine(
            renderer,
            0,            i * CELL_HEIGHT,
            SCREEN_WIDTH, i * CELL_HEIGHT);
    }
}

void
InitSetup()
{
    SDL_Init(SDL_INIT_VIDEO);
    /* The reason calculating min(SCREEN_WIDTH, N*CELL_WIDTH) and
     * min(SCREEN_HEIGHT, N*CELL_HEIGHT) is to ensure that the windows genrated
     * are fully square, i.e. that the 1:1 ratio is not lost.
     */
    SDL_CreateWindowAndRenderer(
        min(SCREEN_WIDTH,  N * CELL_WIDTH),
        min(SCREEN_HEIGHT, N * CELL_HEIGHT),
        SDL_WINDOW_SHOWN, &screen, &renderer
    );

    if (!screen) { LogError("InitSetup faicled to create window"); }

    SetCaption("Game of Life");
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); /* White  */
    SDL_RenderClear(renderer);
    DrawGrid();
    SDL_RenderPresent(renderer);
}

void
render_square(SDL_Renderer* renderer, int row, int column)
{
    const int realRow = row - (row % CELL_WIDTH);
    const int realColumn = column - (column  % CELL_HEIGHT);
    const SDL_Rect my_rect =
        {realRow + 1,
         realColumn + 1,
         CELL_WIDTH - 1,
         CELL_HEIGHT - 1};

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); /* black */
    SDL_RenderFillRect(renderer, &my_rect);
    SDL_RenderPresent(renderer);
}

void
FinishOff()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(screen);
    //Quit SDL
    SDL_Quit();
    exit(0);
}



void
GameLoop()
{
    int gameRunning = 1;
    while(gameRunning)
    {
        while(SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_QUIT: /* if mouse click to close window */
                    gameRunning = 0;
                    break;

                case SDL_MOUSEBUTTONDOWN:
                    render_square(
                        renderer,
                        event.button.x,
                        event.button.y);
                    break;
            }
        }
    }
}

int
main(void){
    InitSetup();
    GameLoop();
    FinishOff();
    return 0;
}
